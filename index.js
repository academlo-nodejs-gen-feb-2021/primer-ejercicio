//Imports de los módulos
const readFileUsers = () => {
    //Imprimir en consola el arreglo de usuarios
};

const writeHelloWorld = () => {
    //Escribir hello world! en el archivo hello.txt

};

const addUser = async (username) => {
    //Agregar un usuario en la lista users.json
};

//No hace falta ejecutar las funciones

module.exports = {
    readFileUsers,
    writeHelloWorld,
    addUser,
};
